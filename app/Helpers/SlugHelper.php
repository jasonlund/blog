<?php
namespace App\Helpers;

class SlugHelper
{
    public function create($model, $title)
    {
        $slug = str_slug($title);

        $repo = app('App\Repositories\\' . $model . 'Repository');

        $count = 1;

        while(true){
            $existing = $repo->findWhere([['slug', '=', $slug]]);

            if(count($existing)){
                $slug = $slug . '-' . $count;
                $count++;
            }else{
                break;
            }
        }

        return $slug;
    }
}