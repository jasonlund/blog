<?php
namespace App\Helpers;

class BladeHelper
{
    public function title(){
        $title = app('App\Repositories\BlogDetailRepository')->findWhere([['key', '=', 'name']])->first();

        return $title->value;
    }
}