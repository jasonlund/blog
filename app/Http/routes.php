<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'posts.index', 'uses' => 'PostsController@index']);

Route::get('/setup', ['as' => 'setup', 'uses' => 'SetupController@index'])->middleware(['setup']);
Route::post('/setup/register', ['as' => 'setup.register', 'uses' => 'Auth\AuthController@postRegister'])->middleware(['setup']);

Route::get('/admin/post/create', ['as' => 'posts.create', 'uses' => 'PostsController@create']);
Route::post('/admin/post/store', ['as' => 'posts.store', 'uses' => 'PostsController@store']);
Route::get('/admin/post/edit/{post}', ['as' => 'posts.edit', 'uses' => 'PostsController@edit']);
Route::post('/admin/post/update/{post}', ['as' => 'posts.update', 'uses' => 'PostsController@update']);

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');