<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BlogDetailRepository
 * @package namespace App\Repositories;
 */
interface BlogDetailRepository extends RepositoryInterface
{
    //
}
