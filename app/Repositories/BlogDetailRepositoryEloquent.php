<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BlogDetailRepository;
use App\Entities\BlogDetail;
use App\Validators\BlogDetailValidator;

/**
 * Class BlogDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BlogDetailRepositoryEloquent extends BaseRepository implements BlogDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BlogDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
