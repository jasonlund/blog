var elixir = require('laravel-elixir');

var folders = {
 'bower': './bower_components/',
 'node': './node_modules/',
 'public': './public/',
 'resources': './resources/assets/'
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix.sass('app.scss');

 mix.scripts([
  folders.bower + 'jquery/dist/jquery.min.js',
  folders.node + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
  folders.bower + 'bootstrap-validator/dist/validator.min.js',
  folders.bower + 'dropzone/dist/min/dropzone.min.js'
 ], folders.public + 'js/vendor.js', './');
});
