@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination-container">
                {!! $posts->render() !!}
            </div>

            <div id="posts-container">
                @include('posts.partials.list', ['posts' => $posts])
            </div>

            <div class="pagination-container">
                {!! $posts->render() !!}
            </div>
        </div>
    </div>
@stop

@section('extra.scripts')
    <script>
        var ajax_list = function(e){
            e.preventDefault();
            if((! $(this).hasClass('disabled')) && (! $(this).hasClass('active'))){
                $.ajax({
                    url: $(this).find('a').attr('href'),
                    type: "GET",
                    dataType : "json"
                })
                .done(function( json ) {
                    $('#posts-container').empty().html(json['html']);
                    $('.pagination-container').empty().html(json['pagination']);
                    $('ul.pagination').find('li').unbind('click', ajax_list).bind('click', ajax_list);
                })
                .fail(function( xhr, status, errorThrown ) {
                    console.log(errorThrown);
                });
            }
        };

        $(document).ready(function(){
            $('ul.pagination').find('li').unbind('click', ajax_list).bind('click', ajax_list);
        });
    </script>
@stop