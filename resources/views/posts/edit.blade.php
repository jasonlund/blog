@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 id="post-form-title">{{ isset($post) ? 'Edit ' . $post->title : 'Create a New Post' }}</h3>

            <div id="form-alert" class="alert alert-dismissible" role="alert" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <form id="post-form" data-toggle="validator">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Post Title</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="Post Title"
                           value="{{ isset($post) ? $post->title : '' }}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="Description"
                           value="{{ isset($post) ? $post->description : '' }}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="10" required>{{ isset($post) ? $post->content : '' }}</textarea>
                    <div class="help-block with-errors"></div>
                </div>
                <button type="submit" id="post-submit-button" class="btn btn-default">Submit</button>
                <a id="post-edit-button" class="btn btn-default btn-info" style="display: none;">Continue to Edit Page</a>
            </form>
        </div>
    </div>
@stop

@section('extra.scripts')
    <script>

        var action = '{{ isset($post) ? 'update' : 'create' }}';

        var post_store_url = '{{ route('posts.store') }}';
        @if(isset($post))
            var post_edit_url = '{{ route('posts.update', ['post' => $post->slug]) }}';
        @endif

        if(action == 'create'){
            var post_url = post_store_url;
        }else{
            var post_url = post_edit_url;
        }

        $('#post-form').validator().on('submit', function (e) {
            if ( ! e.isDefaultPrevented()) {
                e.preventDefault();

                submit_post_form();
            }
        });

        function submit_post_form()
        {
            $.ajax({
                url: post_url,
                data: $('#post-form').serialize(),
                type: "POST",
                dataType : "json"
            })
            .done(function( json ) {
                var alert = $('#form-alert');
                alert.removeClass('alert-danger').addClass('alert-success');
                if(action =='create'){
                    alert.html('Post successfully created.');
                    $('#post-submit-button').hide();
                    $('#post-edit-button').attr('href', '/admin/post/edit/' + json['data']['slug']).show();
                }else{
                    alert.html('Post successfully updated.');
                }
                alert.fadeIn('slow');
            })
            .fail(function( xhr, status, errorThrown ) {
                $('#form-alert').removeClass('alert-success').addClass('alert-danger')
                        .html('There was an issue processing your request.').fadeIn();
                console.log(errorThrown);
            })
        }
    </script>
@stop