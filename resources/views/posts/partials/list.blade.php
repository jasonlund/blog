@foreach($posts as $post)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $post->title }}</h3>
        </div>
        <div class="panel-body">
            {{ $post->description }}
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-lg-4">
                    By:
                </div>
                <div class="col-lg-4">
                    Created: {{ $post->created_at }}
                </div>
                <div class="col-lg-4">
                    Modified: {{ $post->updated_at }}
                </div>
            </div>
        </div>
    </div>
@endforeach