@extends('layout.base')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3>
            Welcome to your blog!
        </h3>
        <p class="lead">Please start by naming your blog and creating your user.</p>
        <form method="POST" action="{{ route('setup.register') }}" data-toggle="validator">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="title">Blog Title</label>
                <input type="text" class="form-control" name="blog_title" id="blog_title"
                       value="{{ old('blog_title') ? old('blog_title') : 'A Three Hour Blog' }}" required>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <label for="title">Your Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Your Name"
                       value="{{ old('name') }}" required>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <label for="title">Your Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address"
                       value="{{ old('email') }}" required>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <label for="title">Password</label>
                <input type="password" class="form-control" name="password" id="password"
                       required>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <label for="title">Password Confirmation</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"
                       required data-match="#password">
                <div class="help-block with-errors"></div>
            </div>

            <div>
                <button class="btn btn-success" type="submit">Create</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('extra.scripts')
@stop